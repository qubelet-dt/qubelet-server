package io.qubelet.server.component

interface ComponentController {
    fun onUpdate()
}