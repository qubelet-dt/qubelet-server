package io.qubelet.server.component

class ComponentArchiType(val components: Set<Class<out Component>>) {
    operator fun iterator(): Iterator<Class<out Component>> = components.iterator()

    fun accepts(holder: ComponentHolder): Boolean = components in holder
}