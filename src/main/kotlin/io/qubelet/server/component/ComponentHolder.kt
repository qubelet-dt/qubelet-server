package io.qubelet.server.component

open class ComponentHolder {
    val components: Set<Component> by lazy {
        hashSetOf<Component>()
    }

    inline fun <reified T : Component> getComponent() : T? = components
            .stream()
            .filter{ it.javaClass == T::class.java}
            .map { it as T }
            .findFirst()
            .orElse(null)

    operator fun contains(component: Class<Component>): Boolean = hasComponent(component)

    inline fun <reified T : Component> addComponent(component: T) = components.plus(component)

    fun hasComponent(type: Class<out Component>) : Boolean = components.filterIsInstance(type).isNotEmpty()

    operator fun contains(archiType: ComponentArchiType): Boolean = archiType.accepts(this)

    operator fun contains(components: Set<Class<out Component>>): Boolean = this.components
            .stream()
            .anyMatch { it::class.java in components }
}