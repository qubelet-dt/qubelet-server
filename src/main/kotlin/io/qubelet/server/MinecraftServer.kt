package io.qubelet.server

class MinecraftServer private constructor() {
    companion object {
        val instance: MinecraftServer = MinecraftServer()
    }
}