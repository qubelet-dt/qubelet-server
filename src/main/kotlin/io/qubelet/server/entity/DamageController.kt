package io.qubelet.server.entity

import io.qubelet.server.component.ComponentArchiType
import io.qubelet.server.component.ComponentController
import io.qubelet.server.world.World

class DamageController : ComponentController {
    override fun onUpdate() {
        val world = World("Default")
        val archiType = ComponentArchiType(setOf(HealthComponent::class.java))

        // Get all entities with ArchiType of currently HealthComponent
        world.getEntities(archiType)

    }
}