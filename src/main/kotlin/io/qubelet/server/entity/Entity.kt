package io.qubelet.server.entity

import io.qubelet.server.component.ComponentHolder
data class Entity(val id: Int) : ComponentHolder()