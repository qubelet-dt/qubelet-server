package io.qubelet.server

import com.github.simplenet.Client
import io.qubelet.server.packet.util.AESCipherPair
import io.qubelet.server.packet.util.CryptoHelper
import java.security.SecureRandom
import java.util.*
import javax.crypto.Cipher

public enum class State {
    HANDSHAKE, LOGIN, STATUS, PLAY
}

data class Session(val client: Client, var state: State = State.HANDSHAKE) {
    var username: String = ""
    var uuid: UUID = UUID.randomUUID()
    val verifyToken = ByteArray(4)
    var sharedSecret: ByteArray = byteArrayOf()
    lateinit var cipherPair: AESCipherPair

    init {
        SecureRandom().nextBytes(verifyToken)
    }

    fun compareVerifyToken(decryption: Cipher, verifyToken: ByteArray): Boolean {
        val decrypted = decryption.doFinal(verifyToken)
        return this.verifyToken.contentEquals(decrypted)
    }

    fun generateAESCipher(decryption: Cipher, secret: ByteArray) : AESCipherPair {
        sharedSecret = decryption.doFinal(secret)
        cipherPair = CryptoHelper.generateAESCiphers(sharedSecret)

        return cipherPair
    }
}