package io.qubelet.server.packet.sent.login

import com.github.simplenet.packet.Packet.builder
import io.qubelet.server.Utils
import io.qubelet.server.packet.Packet
import java.nio.charset.StandardCharsets

data class EncryptionRequestPacket(
        override val id: Int,
        var serverId: String,
        var publicKey: ByteArray = byteArrayOf(),
        var verifyToken: ByteArray = byteArrayOf()
) : Packet {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EncryptionRequestPacket

        if (id != other.id) return false
        if (serverId != other.serverId) return false
        if (!publicKey.contentEquals(other.publicKey)) return false
        if (!verifyToken.contentEquals(other.verifyToken)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + serverId.hashCode()
        result = 31 * result + publicKey.contentHashCode()
        result = 31 * result + verifyToken.contentHashCode()
        return result
    }

    override fun encode(): com.github.simplenet.packet.Packet? {
        val packet = builder()
        packet.putBytes(id.toByte())

        Utils.writeString(serverId, StandardCharsets.UTF_8, packet)
        val publicKeyLen = publicKey.size.toByte()
        val verifyTokenLen = verifyToken.size.toByte()

        packet.putBytes(publicKeyLen)
        packet.putBytes(*publicKey)

        packet.putBytes(verifyTokenLen)
        packet.putBytes(*verifyToken)

        return packet
    }
}

