package io.qubelet.server.packet.sent.login

import com.github.simplenet.packet.Packet.builder
import io.qubelet.server.Utils
import io.qubelet.server.packet.Packet
import java.util.*

data class LoginSuccessPacket(
        override val id: Int,
        val uuid: UUID,
        val username: String
) : Packet {
    override fun encode(): com.github.simplenet.packet.Packet? {
        val packet = builder()
        packet.putBytes(id.toByte())

        Utils.writeString(uuid.toString(), packet)
        Utils.writeString(username, packet)

        return packet
    }
}