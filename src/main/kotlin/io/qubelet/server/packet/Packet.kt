package io.qubelet.server.packet

import com.github.simplenet.packet.Packet
import java.nio.ByteBuffer

interface Packet {
    val id: Int
    fun encode(): Packet? = null
    fun decode(buffer: ByteBuffer) {}
}