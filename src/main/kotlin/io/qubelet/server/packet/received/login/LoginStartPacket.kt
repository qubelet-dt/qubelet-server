package io.qubelet.server.packet.received.login

import io.qubelet.server.Utils
import io.qubelet.server.packet.Packet
import java.nio.ByteBuffer

data class LoginStartPacket(
        override val id: Int,
        var username: String = ""
) : Packet {
    override fun decode(buffer: ByteBuffer) {
        username = Utils.readString(buffer)
    }
}