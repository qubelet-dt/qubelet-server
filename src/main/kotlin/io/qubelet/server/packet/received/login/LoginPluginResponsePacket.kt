package io.qubelet.server.packet.received.login

import io.qubelet.server.Utils
import io.qubelet.server.packet.Packet
import java.nio.ByteBuffer

data class LoginPluginResponsePacket(
        override val id: Int,
        var messageId: Int = 1,
        var successful: Boolean = true,
        var data: ByteArray = byteArrayOf()
) : Packet {
    override fun decode(buffer: ByteBuffer) {
        val packetLen = buffer.remaining()
        messageId = buffer.int
        successful = buffer.get().toInt() == 1

        val messageLen = Utils.writeVarNum(messageId.toLong()).size
        data = ByteArray(packetLen - messageLen)
        buffer.get(data)
    }
}