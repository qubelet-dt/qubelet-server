package io.qubelet.server.packet.received.handshake

import io.qubelet.server.Utils
import io.qubelet.server.packet.Packet
import java.nio.ByteBuffer

public data class ServerListPacket(
        override val id: Int,
        var protocolVersion: Int = 0,
        var serverAddress: String = "",
        var serverPort: Short = 0,
        var nextState: Int = 0
) : Packet {
    override fun decode(buffer: ByteBuffer) {
        protocolVersion = Utils.readVarInt(buffer)
        serverAddress = Utils.readString(buffer)
        serverPort = buffer.short
        nextState = Utils.readVarInt(buffer)
    }
}