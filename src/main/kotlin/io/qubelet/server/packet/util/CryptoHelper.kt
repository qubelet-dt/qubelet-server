package io.qubelet.server.packet.util

import java.security.KeyPairGenerator
import java.security.PrivateKey
import javax.crypto.KeyGenerator

object CryptoHelper {
    val keyGen: KeyPairGenerator = KeyPairGenerator.getInstance("RSA")!!
    init {
        keyGen.initialize(1024)
    }

    fun generateRSAPair() {
        keyGen.generateKeyPair()
    }

    fun generateAESCiphers(secretKey: ByteArray): AESCipherPair = AESCipherPair(secretKey)

    fun generateRSACiphers(key: PrivateKey): RSACipherPair = RSACipherPair(key)
}