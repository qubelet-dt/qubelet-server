package io.qubelet.server.packet.util

import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec

import javax.crypto.spec.SecretKeySpec

data class AESCipherPair(val secretKey: ByteArray) {
    val algorithm = "RSA/ECB/PKCS1Padding"
    val encryption = Cipher.getInstance(algorithm)
    val decryption = Cipher.getInstance(algorithm)

    init {
        val keySpec = SecretKeySpec(secretKey, "AES")
        val parameterSpec = IvParameterSpec(secretKey)

        encryption.init(Cipher.ENCRYPT_MODE, keySpec, parameterSpec)
        decryption.init(Cipher.DECRYPT_MODE, keySpec, parameterSpec)

    }
}