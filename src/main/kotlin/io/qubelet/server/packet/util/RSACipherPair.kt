package io.qubelet.server.packet.util

import java.security.PrivateKey
import javax.crypto.Cipher

class RSACipherPair(privateKey: PrivateKey) {
    val algorithm = "RSA/ECB/PKCS1Padding"
    val encryption = Cipher.getInstance(algorithm)
    val decryption = Cipher.getInstance(algorithm)

    init {
        encryption.init(Cipher.ENCRYPT_MODE, privateKey)
        decryption.init(Cipher.DECRYPT_MODE, privateKey)
    }
}