package io.qubelet.server.packet

import io.qubelet.server.packet.received.handshake.ServerListPacket;
import io.qubelet.server.packet.received.login.*

public enum class ServerBoundPackets(val packets: Map<Int, () -> Packet>) {
    HANDSHAKE(hashMapOf(0 to { ServerListPacket(0) })),
    LOGIN(hashMapOf(0 to { LoginStartPacket(0) }, 1 to { EncryptionResponsePacket(1) }, 2 to { LoginPluginResponsePacket(2) }));

    fun getPacket(opcode: Int): Packet? = packets[opcode]?.invoke()
}