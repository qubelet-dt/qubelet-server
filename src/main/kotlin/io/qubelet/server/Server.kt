package io.qubelet.server

import com.github.simplenet.Client
import com.github.simplenet.Server
import io.qubelet.server.packet.Packet
import io.qubelet.server.packet.ServerBoundPackets
import io.qubelet.server.entity.Entity
import io.qubelet.server.entity.HealthComponent

fun sendPacket(client: Client, data: Packet) {
    data.encode()?.queue(client)
    client.flush()
}

fun handlePacket(client: Client, session: Session, packetLength: Int): Session {
    Utils.readVarInt(client) { opcode ->
        client.read(packetLength - Utils.writeVarNum(opcode.toLong()).size) { buffer ->
            when (session.state) {
                State.HANDSHAKE -> {
                    val packet = ServerBoundPackets.HANDSHAKE.getPacket(opcode)!!
                    packet.decode(buffer)
                    println(packet)

                    when (opcode) {
                        0 -> {
                            session.state = State.LOGIN
                        }
                    }
                }
                State.LOGIN -> {
                    val packet = ServerBoundPackets.LOGIN.getPacket(opcode)!!
                    packet.decode(buffer)
                    println(packet)
                }
            }
        }
    }
    return session
}

fun main() {
    val server: Server = Server()
    MinecraftServer.instance

    val entity = Entity(25)
    entity.addComponent(HealthComponent())

    server.onConnect { client ->
        var session = Session(client)

        Utils.readVarIntAlways(client) { packetLength ->
            session = handlePacket(client, session, packetLength)
        }
    }

    server.bind("localhost", 25565);
}