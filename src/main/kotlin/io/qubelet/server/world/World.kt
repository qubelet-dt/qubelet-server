package io.qubelet.server.world

import io.qubelet.server.component.ComponentArchiType
import io.qubelet.server.entity.Entity
import java.util.stream.Collectors

data class World(
        val name: String,
        val entities: HashSet<Entity> = hashSetOf()
) {
    fun onTick() {}

    fun getEntities(archiType: ComponentArchiType): Set<Entity> {
        return entities.stream().filter {archiType in it}.collect(Collectors.toSet())
    }
}