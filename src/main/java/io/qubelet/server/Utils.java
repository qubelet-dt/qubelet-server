package io.qubelet.server;

import com.github.simplenet.Client;
import com.github.simplenet.packet.Packet;
import com.github.simplenet.utility.exposed.consumer.ByteConsumer;
import com.github.simplenet.utility.exposed.predicate.BytePredicate;
import com.google.common.primitives.Longs;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.function.LongConsumer;

import static kotlin.text.Charsets.UTF_8;

public class Utils {
    private static void readVarNum(Client client, int maxBytes, LongConsumer callback) {
        client.readByteUntil(new BytePredicate() {
            int count;
            long value;

            @Override
            public boolean test(byte data) {
                value |= ((int) data & 0x7F) << (7 * count);
                if (++count > maxBytes) throw new RuntimeException("VarNum is too big");
                if ((data & 0x80) == 0) {
                    callback.accept(value);
                    return false;
                } else return true;
            }
        });
    }

    private static long readVarNum(ByteBuffer byteBuffer, int maxBytes) {
        int count = 0;
        long value = 0;

        while (true) {
            byte data = byteBuffer.get();
            value |= ((int) data & 0x7F) << (7 * count);
            if (++count > maxBytes) throw new RuntimeException("VarNum is too big");
            if ((data & 0x80) == 0) {
                return value;
            }
        }
    }

    public static byte[] writeVarNum(long value) {
        return Longs.toByteArray(value);
    }

    public static void readString(Client client, Consumer<String> callback) {
        readVarInt(client, stringLength ->
                client.readBytes(stringLength, bytes ->
                        callback.accept(new String(bytes, UTF_8))
                )
        );
    }

    public static String readString(ByteBuffer byteBuffer) {
        int stringLength = readVarInt(byteBuffer);
        byte[] bytes = new byte[stringLength];
        byteBuffer.get(bytes);
        return new String(bytes, UTF_8);
    }

    public static void readVarInt(Client client, IntConsumer callback) {
        readVarNum(client, 5, it -> callback.accept((int) it));
    }

    public static void readVarLong(Client client, LongConsumer callback) {
        readVarNum(client, 10, callback);
    }

    public static int readVarInt(ByteBuffer byteBuffer) {
        return (int) readVarNum(byteBuffer, 5);
    }

    public static long readVarLong(ByteBuffer byteBuffer) {
        return readVarNum(byteBuffer, 10);
    }

    public static void readVarIntAlways(Client client, IntConsumer callback) {
        client.readByteAlways(new ByteConsumer() {
            int count;
            long value;

            @Override
            public void accept(byte data) {
                value |= ((int) data & 0x7F) << (7 * count);
                if (++count > 5) throw new RuntimeException("VarNum is too big");
                if ((data & 0x80) == 0) {
                    callback.accept((int) value);
                    count = 0;
                    value = 0;
                }
            }
        });
    }

    public static void writeLegacyString(String string, Charset charset, Packet packet) {
        final int length = string.length();
        packet.putShort(length);
        packet.putBytes(string.getBytes(charset));
    }

    public static void writeLegacyString(String string, Packet packet) {
        writeLegacyString(string, UTF_8, packet);
    }

    public static void writeString(String string, Charset charset, Packet packet) {
        final byte[] bytes = string.getBytes(charset);
        final int length = bytes.length;
        final byte[] strLen = writeVarNum(length);
        packet.putBytes(strLen);
        packet.putBytes(bytes);
    }

    public static void writeString(String string, Packet packet) {
        writeString(string, UTF_8, packet);
    }

}
